let tabsShow =$(function() {
        $(".tabs").on("click", "li:not(.active-tab)", function() {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active-tab")
                .closest(".container-3")
                .find(".tabs-panel")
                .removeClass("active")
                .eq($(this).index())
                .addClass("active");
        });
});
