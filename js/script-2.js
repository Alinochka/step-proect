let mainImgBox = $( document ).ready(function () {
    $(".img-box-hidden").slice(0, 12).show();

    $(".tabs-work").on("click", "li:not(.active-amazing-work)", function() {
        $(this)
            .siblings()
            .removeClass("active-amazing-work")
    });

 let showImg = $(".tabs-btn-work").click(function(){
        let value = $(this).attr("id");
        let elem = $(".img-box-hidden");
        if(value === "all-amazing-work"){
            $(elem).show("12");
        }
        else{
            $(elem).not("."+value).hide("12");
            $(elem).filter("."+value).show("12");
        }
    });
    
 let loadMore = $(".work-main-btn").on('click', function (e) {
        e.preventDefault();
        $(".img-box-hidden:hidden").slice(0, 12).slideDown();

        $('.container-5').animate({
                scrollTop: $(this).offset().top
            }, 1500);
     $(".work-main-btn").remove();
 });
});


