let slider = $(function() {
    $(".celebrity").hide().first().show();
    $(".slider-photo").click(function(){
        $(this)
            .addClass("active-slider-photo")
            .siblings()
            .removeClass("active-slider-photo");
        mainSlider();
        return slider;
    });

    let Switch;
    let newSwitch;
    
let rightSwitches = $(".slider-switches.right").click(function(){
        Switch = $(".slider-photo.active-slider-photo").index() - 1;
        newSwitch = (Switch == 3) ? 0 : Switch + 1;
        $(`.slider-photo:eq(${newSwitch})`)
            .addClass("active-slider-photo")
            .siblings()
            .removeClass("active-slider-photo");
        mainSlider();
        return  rightSwitches;
    });

let leftSwitches = $(".slider-switches.left").click(function(){
         Switch = $(".slider-photo.active").index() - 1;
         newSwitch = (Switch == 0) ? 3 :Switch- 1;
        $(`.slider-photo:eq(${newSwitch})`)
            .addClass('active-slider-photo')
            .siblings()
            .removeClass('active-slider-photo');
        mainSlider();
    return leftSwitches;
    });

function mainSlider () {
         Switch =  $(".slider-photo.active-slider-photo").index() - 1;
        $(`.celebrity:eq(${Switch})`)
            .show()
            .siblings('.celebrity')
            .hide();
    }
});
